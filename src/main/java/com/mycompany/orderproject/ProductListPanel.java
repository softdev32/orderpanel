/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.orderproject;

import java.awt.GridLayout;
import java.util.ArrayList;

/**
 *
 * @author Windows10
 */
public class ProductListPanel extends javax.swing.JPanel implements OnBuyProductListener{
    
    private ArrayList<OnBuyProductListener>subscriberList=new ArrayList<>();
    private final ArrayList<Product> productList;
    private OrderPanel orderPanel;

    /**
     * Creates new form ProductListPanel
     */
    public ProductListPanel() {
        initComponents();
        productList=Product.getMockProductList();
        
        for(Product product:productList){
            ProductPanel panel=new ProductPanel(product);
            productPanel.add(panel);
            panel.addOnBuyListener(this);
        }
        int num =productPanel.getComponentCount();
        int col=3;
        productPanel.setLayout(new GridLayout(num/col+(num%col>0?1:0),col));
    }
    public void addOnBuyListener(OnBuyProductListener subscriber){
        subscriberList.add(subscriber);
    }
    public void setOrderPanel(OrderPanel orderpanel){
        this.orderPanel=orderPanel;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scrPanel = new javax.swing.JScrollPane();
        productPanel = new javax.swing.JPanel();

        javax.swing.GroupLayout productPanelLayout = new javax.swing.GroupLayout(productPanel);
        productPanel.setLayout(productPanelLayout);
        productPanelLayout.setHorizontalGroup(
            productPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 586, Short.MAX_VALUE)
        );
        productPanelLayout.setVerticalGroup(
            productPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 476, Short.MAX_VALUE)
        );

        scrPanel.setViewportView(productPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 588, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 478, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel productPanel;
    private javax.swing.JScrollPane scrPanel;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Product product, int amount) {
//        orderPanel.buy(product, amount);
          for(OnBuyProductListener s:subscriberList){
              s.buy(product,amount);
          }
    }
}
